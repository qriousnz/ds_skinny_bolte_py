#! /usr/bin/env python3

"""
Produces list of active Skinny customers whose home location lies within
broadband over LTE (BOLTE) coverage. Coverage is supplied in the form of a kmz
file (zipped kml).

TODO (if this code is ever reused) - csm --> cell_location, use LSR-derived LBS
to identify home location (only case in which looking at distance to boundary
actually makes any sense - otherwise too much error in home location to allow
such precision)
"""

import datetime
from random import sample

from qat import db, settings, spatial, utils

SCHEMA = 'skinny_bolte'
ACTIVE_CUSTOMERS_TPL = '{}.active_customers{}'
BOLTE_TPL = '{}.bolte_cover{}'
LATEST_HOME_LOC = 'grantps_test.latest_home_loc'
date_str_made = datetime.datetime.now().strftime('%Y%m%d')
OUTPUT_CSV = 'data/output/skinny_bolte_{}.csv'.format(date_str_made)
COMPARISON_DB = '/home/gps/projects/skinny_bolte/data/output/outputs.db'

def _get_current_month_year():
    now = datetime.datetime.today()
    month = int(now.strftime('%m'))
    year = int(now.strftime('%Y'))
    return month, year

def _get_month_suffix():
    """
    Get month suffix for adding to table name
    e.g. skinny_matched_active_cust_2017_11 (November 2017)
    """
    month, year = _get_current_month_year()
    if month == 1:
        month = 12
        year -= 1
    else:
        month -= 1
    month_suffix = "_{}_{:02}".format(year, month)
    return month_suffix

def _get_last_date_prev_month():
    month, year = _get_current_month_year()
    start_current_month = datetime.date(year, month, 1)
    last_date_prev_month = start_current_month - datetime.timedelta(days=1)
    return last_date_prev_month

def get_cust_dets(active_date):
    unused, cur = db.Pg.skinny()
    ## sql based on skinny_extract_4R.sql
    sql = """\
    SELECT DISTINCT
      activity.customer_key, 
      customer.subscriber_id,
      dim.imsi,
        activity.activity_start_dtm::date AS
      activity_start_dtm
    FROM dwh.pivot_fact_Customer_activity AS activity
    INNER JOIN skinny_wk.skinny_cust_imsi AS dim 
      ON activity.customer_key = dim.customer_key
    INNER JOIN dwh.dim_customer AS customer
      ON activity.customer_key = customer.customer_key
    WHERE activity.activity_type = 'ACTIVE' 
    AND %s BETWEEN activity.activity_start_dtm::date
      AND activity.activity_end_dtm::date -- note - there is always an end date set
    AND dim.imsi IS NOT NULL
    """
    cur.execute(sql, (active_date, ))
    data = cur.fetchall()
    return data

def check_customer_dets(customer_key):
    unused, cur = db.Pg.skinny()
    sql = """\
    SELECT DISTINCT
      activity.customer_key, 
      customer.subscriber_id,
      dim.imsi,
      activity.activity_type,
        SUBSTRING(activity.activity_start_dtm::text, 1, 10) AS
      activity_start_dtm,
        SUBSTRING(activity.activity_end_dtm::text, 1, 10) AS
      activity_end_dtm
    FROM dwh.pivot_fact_Customer_activity AS activity
    INNER JOIN skinny_wk.skinny_cust_imsi AS dim 
      ON activity.customer_key = dim.customer_key
    INNER JOIN dwh.dim_customer AS customer
      ON activity.customer_key = customer.customer_key
    WHERE customer.customer_key = %s
    ORDER BY activity_start_dtm
    """
    cur.execute(sql, (customer_key, ))
    data = cur.fetchall()
    for row in data:
        print(row)

def make_active_customers_tbl(month_suffix):
    ## get customer details
    last_date_prev_month = _get_last_date_prev_month()
    cust_dets = get_cust_dets(active_date=last_date_prev_month)
    ## make CSV
    csv_tpl = "{} , {} , {} , {}, {}\n".replace(' ', '')
    src_csv_fpath = 'data/working/active_customers.csv'
    n_tot = len(cust_dets)
    with open(src_csv_fpath, 'w') as f:
        for n, row in enumerate(cust_dets, 1):
            imsi_rti_hex = utils.Imsi.imsi2imsi_hex(row['imsi'])
            f.write(csv_tpl.format(*row, imsi_rti_hex))
            if n % 20000 == 0:
                print(utils.prog(n, label='active customers', tot=n_tot))
    dest_tblname=ACTIVE_CUSTOMERS_TPL.format(SCHEMA, month_suffix)
    dest_fields = ['customer_key', 'subscriber_id', 'imsi',
        'activity_start_dtm', 'imsi_rti_hex']
    ## make dest table
    con, cur = db.Pg.gp(user=settings.GP_USER_ADMIN)
    db.Pg.drop_tbl(con, cur, dest_tblname)
    sql = """\
    CREATE TABLE {} (
      customer_key INT,
      subscriber_id BIGINT,
      imsi BIGINT,
      activity_start_dtm DATE,
      imsi_rti_hex TEXT
    )
    """.format(dest_tblname)
    cur.execute(sql)
    con.commit()
    db.Pg.grant_public_read_permissions(dest_tblname)
    ## transfer CSV to GP to allow joining to location data and spatial analysis
    db.Pg.csv2tbl(src_csv_fpath,
        dest_tblname, dest_fields,
        dest_user=settings.GP_USER_ADMIN, dest_pwd=settings.GP_PWD_ADMIN,
        has_header=False)
    print("Made '{}'".format(dest_tblname))

def make_bolte_geom_str_tbl(month_suffix, kmz_fpath, display=True):
    con, cur = db.Pg.gp(user=settings.GP_USER_ADMIN)
    bolte_cover = BOLTE_TPL.format(SCHEMA, month_suffix)
    db.Pg.drop_tbl(con, cur, tblname=bolte_cover)
    con.close()  ## timeout issue
    geom_str = spatial.Spatial.kmz2geom_strs(kmz_fpath, display=display)[0]
    con, cur = db.Pg.gp(user=settings.GP_USER_ADMIN)
    sql_make_tbl = """\
    CREATE TABLE {bolte_cover} (
      bolte_cover TEXT
    )
    """.format(bolte_cover=bolte_cover)
    cur.execute(sql_make_tbl)
    con.commit()
    sql_insert = """\
    INSERT INTO {bolte_cover}
    (bolte_cover)
    VALUES (%s)
    """.format(bolte_cover=bolte_cover)
    cur.execute(sql_insert, (geom_str, ))
    con.commit()
    db.Pg.grant_public_read_permissions(bolte_cover)
    print("Made '{}'".format(bolte_cover))

def update_home_locations():
    """
    Based on run src/home_loc_refresh.sh

    Looks like GP (vs Hive) lbs_agg.fact_subscriber_home_location hasn't been
    updated since 2017-05 :-(

    So run it on Hive and transfer across.
    """
    print("About to make '{}' ...".format(LATEST_HOME_LOC))
    con_hive, cur_hive = db.Hive.hive()
    db.Hive.drop_tbl(con_hive, cur_hive, tblname=LATEST_HOME_LOC)
    sql = """\
    CREATE TABLE {latest_home_loc} AS
    SELECT *
    FROM
    (
    SELECT
        imsi_rti,
        cell_id_rti,
        cell_prefix_hours,
        month,
          MAX(month) OVER (PARTITION BY imsi_rti) AS
        max_month
    FROM lbs_agg.fact_subscriber_home_location
    ) AS src
    """.format(latest_home_loc=LATEST_HOME_LOC)
    cur_hive.execute(sql, async=True)
    db.Hive.fetch_prog(cur_hive)
    con_hive.commit()
    print("Made '{}' in Hive".format(LATEST_HOME_LOC))
    ## transfer to GP ready for PostGIS analysis
    #input("Run\nhive2gpdb --hschema grantps_test --htable latest_home_loc "
    #    "--gschema master --gtable latest_home_loc\nbefore proceeding")
    db.hive2gp(hive_username=settings.HIVE_USER,
        hschema='grantps_test', htable='latest_home_loc',
        gschema='master', gtable='latest_home_loc')
    con_gp, cur_gp = db.Pg.gp(user=settings.GP_USER_ADMIN)
    db.Pg.drop_tbl(con_gp, cur_gp, tblname=LATEST_HOME_LOC)
    sql_master2gps = """\
    CREATE TABLE grantps_test.latest_home_loc
    AS
    SELECT *
    FROM master.latest_home_loc
    """
    cur_gp.execute(sql_master2gps)
    con_gp.commit()
    print("Made '{}' in Greenplum".format(LATEST_HOME_LOC))

def get_active_skinny_dets(month_suffix):
    """
    Uses cell_sector_master so on the back foot right there ;-). Leaving
    unchanged because possibly not long-term code. Fix if it has more of a
    future.

    Note sure why original code changed unified lat and lon into NZGD2000.

    Also noted that 
    """
    debug = True
    unused, cur = db.Pg.gp(user=settings.GP_USER_ADMIN)
    active_customers = ACTIVE_CUSTOMERS_TPL.format(SCHEMA, month_suffix)
    bolte_cover = BOLTE_TPL.format(SCHEMA, month_suffix)
    ## sql based on geom_worx_4R.sql
    sql = """\
    SELECT
      customer_key,
      subscriber_id,
      latitude,
      longitude,
      city,
      suburb,
      postcode,
      region,
      tower,
      cell_prefix_hours,
        SUBSTRING(home_loc_mth::text, 1, 10) AS
      home_loc_mth,
        ST_INTERSECTS(h_point, bolte_cover) AS
      bolte_coverage,
        100000*ST_DISTANCE(
          h_point,
          ST_CLOSESTPOINT(bolte_boundary, h_point)
          ) AS
      boundary_dist_m
    FROM
      (
      SELECT
        customer_key,
        subscriber_id,
        activity_start_dtm,
          (month||'-01')::date - interval '1 month' AS
        home_loc_mth,
        cell_prefix_hours,
          unified_latitude AS
        latitude,
          unified_longitude AS
        longitude,
        city,
        suburb,
          ST_MAKEPOINT(unified_longitude, unified_latitude) AS
        h_point,
        region,
        postcode,
        tower
      FROM {active_customers} AS skinny
      LEFT JOIN lbs_raw.dim_subscriber AS dim
        on skinny.imsi_rti_hex = dim.imsi_rti_hex
      LEFT JOIN
        (
        SELECT
          imsi_rti,
          cell_id_rti,
          cell_prefix_hours,
          month,
            MAX(month) OVER (PARTITION BY imsi_rti) AS
          max_month
        FROM {latest_home_loc}
        ) AS home
      ON dim.imsi_rti = home.imsi_rti
      LEFT JOIN master.cell_sector_master AS csm
        ON home.cell_id_rti = csm.cell_id_rti
      WHERE month = max_month  -- breaks the LJness and means home might shrink data down if missing recent records
      ) AS mskinny,
      (
        SELECT
        bolte_cover::geometry,
          ST_BOUNDARY(bolte_cover::geometry) AS
        bolte_boundary
        FROM {bolte_cover}
      ) AS bolte
    """.format(active_customers=active_customers, bolte_cover=bolte_cover,
    latest_home_loc=LATEST_HOME_LOC)
    if debug:
        now = datetime.datetime.now().strftime('%H:%M:%S')
        print("About to run query getting customer data. Could take a couple of"
        " hours. Started at {}".format(now))
    cur.execute(sql)
    if debug: print("About to fetch data from query")
    data = cur.fetchall()
    return data

def make_output_csv(active_skinny_dets):
    csv_tpl = "\n{},{},{},{},{},{},{},{},{},{},{},{},{}"
    with open(OUTPUT_CSV, 'w') as f:
        f.write("customer_key,subscriber_id,latitude,longitude,city,suburb,"
            "postcode,region,tower,cell_prefix_hours,home_loc_mth,"
            "bolte_coverage,boundary_dist_m")
        n_tot = len(active_skinny_dets)
        for n, row in enumerate(active_skinny_dets, 1):
            f.write(csv_tpl.format(*row))
            if n % 10000 == 0:
                print(utils.prog(n, label='active skinny details', tot=n_tot))
    print("Finished - made output '{}'".format(OUTPUT_CSV))

def get_bolte_customer_list(kmz_fpath):
    checking_only = False
    if not checking_only:
        #month_suffix = _get_month_suffix()
        month_suffix = '_2017_12'
        #make_active_customers_tbl(month_suffix)
        #make_bolte_geom_str_tbl(month_suffix, kmz_fpath, display=False)
        #update_home_locations()
        active_skinny_dets = get_active_skinny_dets(month_suffix)
        make_output_csv(active_skinny_dets)

def compare_kmzs(kmz_fpath_a, kmz_fpath_b):
    #spatial.Spatial.kmz2geom_strs(kmz_fpath_a, display=True, chunk_size=1000)
    spatial.Spatial.kmz2geom_strs(kmz_fpath_b, display=True, chunk_size=200)

def _get_pct_in_coverage_zone(cur, tblname):
    sql = """\
    SELECT
      bolte_coverage,
        CASE
            WHEN bolte_coverage = 'True' THEN 'YES'
            WHEN bolte_coverage = 'False' THEN 'No'
          END AS
      coverage,
        COUNT(*) AS
      freq,
        ROUND(
          (COUNT(*)*100.0)
          /
          (SELECT COUNT(*) AS tot FROM {tblname}),
          2) AS
      pct
    FROM {tblname}
    GROUP BY bolte_coverage
    """.format(tblname=tblname)
    cur.execute(sql)
    data = cur.fetchall()
    return data

def _get_changes(cur, tbl_a, tbl_b):
    """
    How many changed from inside coverage to outside and v.v.? Return samples.
    """
    SAMPLE_SIZE_GAINED = 10
    SAMPLE_SIZE_LOST = 200  ## more problematic
    sql_tpl = """\
    SELECT
      a.customer_key,
      a.latitude AS lat_a, a.longitude AS lon_a,
      b.latitude AS lat_b, b.longitude AS lon_b,
      a.boundary_dist_m AS boundary_dist_a,
      b.boundary_dist_m AS boundary_dist_b,
      a.suburb AS suburb_a,
      b.suburb AS suburb_b,
      a.city AS city_a,
      b.city AS city_b
    FROM {tbl_a} AS a
    INNER JOIN
    {tbl_b} AS b
    USING(customer_key)
    {{WHERE_clause}}
    """.format(tbl_a=tbl_a, tbl_b=tbl_b)
    WHERE_gained = ("WHERE a.bolte_coverage = 'False' "
        "AND b.bolte_coverage = 'True'")
    sql_gained = sql_tpl.format(WHERE_clause=WHERE_gained)
    WHERE_lost = ("WHERE a.bolte_coverage = 'True' "
        "AND b.bolte_coverage = 'False'")
    sql_lost = sql_tpl.format(WHERE_clause=WHERE_lost)
    cur.execute(sql_gained)
    data_gained_coverage = cur.fetchall()
    n_gained_coverage = len(data_gained_coverage)
    sample_gained_coverage = sample(data_gained_coverage, SAMPLE_SIZE_GAINED)
    cur.execute(sql_lost)
    data_lost_coverage = cur.fetchall()
    n_lost_coverage = len(data_lost_coverage)
    sample_lost_coverage = sample(data_lost_coverage, SAMPLE_SIZE_LOST)
    return (n_gained_coverage, n_lost_coverage,
        sample_gained_coverage, sample_lost_coverage)

def examine_changes(tbl_a, tbl_b):
    debug = False
    unused, cur = db.Sqlite.get_con_cur(COMPARISON_DB)
    results = _get_changes(cur, tbl_a, tbl_b)
    (n_gained_coverage, n_lost_coverage,
        sample_gained_coverage, sample_lost_coverage) = results
    print("n_gained_coverage: {:,}".format(n_gained_coverage))
    print("n_lost_coverage: {:,}".format(n_lost_coverage))
    data_dets = (
        ('Gained coverage', sample_gained_coverage),
        ('Lost coverage', sample_lost_coverage)
    )
    for lbl, data in data_dets:
        print("\n\n{}\n".format(lbl))
        for row in data:
            if debug: print(dict(row))
            customer_key = row['customer_key']
            coord_a = (float(row['lat_a']), float(row['lon_a']))
            coord_b = (float(row['lat_b']), float(row['lon_b']))
            gap_km = spatial.Spatial.gap_km(coord_a, coord_b)
            if gap_km == 0:
                gap_km = 0
            boundary_dist_a = round(float(row['boundary_dist_a']), 4)
            boundary_dist_b = round(float(row['boundary_dist_b']), 4)
            suburb_a = row['suburb_a']
            suburb_b = row['suburb_b']
            city_a = row['city_a']
            city_b = row['city_b']
            if suburb_a == 'None' or suburb_b == 'None':
                if city_a == 'None' or city_b == 'None':
                    loc_msg = 'N/A'
                elif city_a == city_b:
                    loc_msg = 'SAME city ({})'.format(city_a)
                else:
                    loc_msg = "{} then {}".format(city_a, city_b)
            elif suburb_a == suburb_b:
                loc_msg = "SAME suburb ({})".format(suburb_a)
            else:
                loc_msg = "{} then {}".format(suburb_a, suburb_b)
            if gap_km:
                coords_msg = ""
            else:
                coords_msg = "{} (check old and new maps)".format(coord_a)
            shift = "{:,}km".format(gap_km) if gap_km else ''
            print("id: {:>11}  Shifted: {:>9}  boundary_dist: A {:,} B {:,}"
                "   {}  {}".format(customer_key, shift,
                boundary_dist_a, boundary_dist_b,
                loc_msg, coords_msg))
    return

    for tblname in (tbl_a, tbl_b):
        print(tblname)
        data = _get_pct_in_coverage_zone(cur, tblname)
        for row in data:
            print(dict(row))

def main():
    #kmz_fpath_a = "data/coverage/Skinny_BB_Coverage_20160831.kmz"
    #kmz_fpath_b = "data/coverage/WBB_Coverage_20171215.kmz"
    #compare_kmzs(kmz_fpath_a, kmz_fpath_b)
    #get_bolte_customer_list(kmz_fpath_b)
    tbl_a = 'sb_20180115'  ## run recently but using kmz_fpath_a (urban-only)
    tbl_b = 'sb_20180125'  ## run recently using kmz_fpath_b (all NZ)
    examine_changes(tbl_a, tbl_b)

if __name__ == '__main__':
    main()
