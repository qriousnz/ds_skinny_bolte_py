# README #

* 1.2.0

Basically the Python version of https://bitbucket.org/qriousnz/ds_skinny_bolte
(R and bash). Added the region, postcode, tower fields by customer request.

### What is this repository for? ###

* Generate list of Skinny customers in Bolte area (and some nearby) from a
KMZ input

### How do I get set up? ###

* Need Python3
* Install qat - https://bitbucket.org/qriousnz/qat

### How do I generate the CSV? ###

* Store KMZ file on file system where you can access it
* Connect to VPN (must have access to cdh server)
* Update line in main function in skinny_bolte_list.py defining location of KMZ
  e.g. kmz_fpath = "data/coverage/Skinny_BB_Coverage_20160831.kmz"
* Run skinny_bolte_list.py
* Open CSV file as defined in output e.g.
  Finished - made output 'data/output/skinny_bolte_20180115.csv'

### Who do I talk to? ###

* for praise - Grant Paton-Simpson (python implementation)
* for blame - Ivan Rivera (original design and logic)

### Misc developer notes ###

Other approaches - could use shapely instead of SQL and look at intersections
with individual home location coords. Why use home location coordinates instead
of those we can (somehow ;-)) base on customer address?

Even in coverage increases overall it is possible for customers to stay in the
same location and lose coverage. The coverage maps are really areas BOLTE can be
sold into and if an area gets congested they have to stop selling until more
support is supplied.

It can be better to display nationwide coverage by supplying folium with
50 geojson_strings rather than one massive one. It has the same impact on the
browser but the act of generating the geom_strs can be much, much, much slower.
Think 1.25 hours versus days. Interestingly, the 20160831 urban-only coverage
was generated in 3 minutes from 8,000 objects whereas the 20171215 NZ coverage
took, um, it's probably still running ...
